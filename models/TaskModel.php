<?php

require_once('Model.php');

class TaskModel extends Model {

    /**
     * @return array
     * Retorna todas las tareas almacenadas en la tabla task
     */
    function getAll() {

        $query = $this->getDb()->prepare('SELECT * FROM task ORDER BY priority ASC');
        $query->execute();
        return $query->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * @param $id
     * @return mixed
     * Retorna una tupla a partir de un id pasado por parámtro
     */
    function get($id) {

        $query = $this->getDb()->prepare('SELECT * FROM task WHERE id = ?');
        $query->execute(array(($id)));
        return $query->fetch(PDO::FETCH_OBJ);
    }    

    /**
     * @param $title, $priority, $description
     * Crea una tarea a partir de los parámetros title, priority, description e image
     */
    function new($title, $priority, $description, $image = null) {
        $pathImg = null;
        if ($image)
            $pathImg = $this->uploadImage($image);
        
        $query = $this->getDb()->prepare('INSERT INTO task (title, priority, description, completed, image) VALUES (?, ?, ?, false, ?)');
        return $query->execute([$title, $priority, $description, $pathImg]);
    }

    /**
     * @param $image
     * Genera un nombre único para la imagen y la copia, finalmente retorna el path de la misma
     */
    function uploadImage($image) {
        $target = 'uploads/tasks/' . uniqid() . '.jpg';
        move_uploaded_file($image, $target);
        return $target;
    }

    /**
     * @param $id
     * Elimina una tarea en base al id pasado por parámetro
     */
    function delete($id) {

        $query = $this->getDb()->prepare('DELETE FROM task WHERE id = ?');
        $query->execute([$id]);
    }

    /**
     * @param $id
     * Elimina una tarea en base al id pasado por parámetro
     */
    function end($id) {

        $query = $this->getDb()->prepare('UPDATE task SET completed = true WHERE id = ?');
        $query->execute([$id]);
    }
}


