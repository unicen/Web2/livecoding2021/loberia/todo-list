<?php
/* Smarty version 3.1.39, created on 2021-05-04 21:56:14
  from '/var/www/html/todo-list/templates/taskDetail.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6091c2fe066ca2_76317076',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4c9a4c49733c3d7b2898b3edc89d64a2b8b6c3ef' => 
    array (
      0 => '/var/www/html/todo-list/templates/taskDetail.tpl',
      1 => 1620165372,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6091c2fe066ca2_76317076 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
    </head>
    
    <body>
        <h1><?php echo $_smarty_tpl->tpl_vars['task']->value->title;?>
 (<?php echo $_smarty_tpl->tpl_vars['task']->value->priority;?>
)</h1>
        <p><?php echo $_smarty_tpl->tpl_vars['task']->value->description;?>
</p>
    </body>
</html>
<?php }
}
