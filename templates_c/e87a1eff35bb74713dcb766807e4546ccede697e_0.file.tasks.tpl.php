<?php
/* Smarty version 3.1.39, created on 2021-05-18 21:33:50
  from '/var/www/html/todo-list/templates/tasks.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_60a432beaea851_69639603',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e87a1eff35bb74713dcb766807e4546ccede697e' => 
    array (
      0 => '/var/www/html/todo-list/templates/tasks.tpl',
      1 => 1621373044,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:templates/header.tpl' => 1,
    'file:templates/formulario.tpl' => 1,
    'file:templates/footer.tpl' => 1,
  ),
),false)) {
function content_60a432beaea851_69639603 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:templates/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:templates/formulario.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<ul>

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['tasks']->value, 'task');
$_smarty_tpl->tpl_vars['task']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['task']->value) {
$_smarty_tpl->tpl_vars['task']->do_else = false;
?> 
        <?php if ($_smarty_tpl->tpl_vars['task']->value->completed) {?>
            <li class="list-group-item item completed">
        <?php } else { ?>
            <li class="list-group-item">
        <?php }?>

        <?php echo $_smarty_tpl->tpl_vars['task']->value->title;?>
 <small><a href="task/<?php echo $_smarty_tpl->tpl_vars['task']->value->id;?>
"> Details </a></small>
        <?php echo $_smarty_tpl->tpl_vars['task']->value->title;?>
 <small><a href="delete/<?php echo $_smarty_tpl->tpl_vars['task']->value->id;?>
"> Delete </a></small>
        <?php if ((!$_smarty_tpl->tpl_vars['task']->value->completed)) {?>
            <?php echo $_smarty_tpl->tpl_vars['task']->value->title;?>
<small><a href="completed/<?php echo $_smarty_tpl->tpl_vars['task']->value->id;?>
"> Completed </a></small>
        <?php }?>
        </li>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

</ul>
    
<?php $_smarty_tpl->_subTemplateRender('file:templates/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
