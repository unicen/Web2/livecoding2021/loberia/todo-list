<?php

define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');

require_once('controllers/TaskController.php');
require_once('controllers/UserController.php');

if ($_GET['action'] == '')
	$_GET['action'] = 'home';

$urlParts = explode('/', $_GET['action']);
$taskController = new TaskController();
$userController = new UserController();

switch ($urlParts[0]) {
	case 'task':
		$taskController->showDetail($urlParts[1]);
		break;
	case 'delete':
		$taskController->delete($urlParts[1]);
		break;
	case 'new':
		$taskController->newTask();
		break;
	case 'completed':
		$taskController->endTask($urlParts[1]);
		break;
	case 'home':
		$taskController->showTasks();
		break;
	case 'tasks':
		$taskController->showTasks();
		break;
	case 'login':
		$userController->showLogin();
		break;
	case 'formRegistro':
		$userController->showRegistrar();
		break;
	case 'registrar':
		$userController->registrar();
		break;
	case 'verify':
		$userController->verify();
		break;
	case 'logout':
		$userController->logout();
	default:
		echo '<h1>Error 404 - Page not found </h1>';
		break;
}