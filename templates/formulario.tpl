<div class="form-style">
        <h1>{$title}</h1>

        <form action="new" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label>Title</label>
                <input type="text" name="title" class="form-control">
            </div>

            <div class="form-group">
                <label>Priority</label>
                <select name="priority" class="form-control">
                    <option value="1">Priority 1</option>
                    <option value="2">Priority 2</option>
                    <option value="3">Priority 3</option>
                </select>
            </div>

            <div class="form-group">
                <label>Description</label>
                <textarea type="text" name="description" class="form-control"></textarea>
            </div>

            <div class="form-group">
                <input type="file" name="input_name" id="imageToUpload">
            </div>

            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>