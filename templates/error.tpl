{include 'templates/header.tpl'}

<div class='text-center'>
    <h2>Error</h2>
    <h5>{$message}</h5>
    <img src='imgs/icon_task.png'>
</div>
<div class="text-center"><a class="" href="{$home}">Back</a></div>

{include 'templates/footer.tpl'}
