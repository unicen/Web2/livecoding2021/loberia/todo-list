{include 'templates/header.tpl'}

{include 'templates/formulario.tpl'}

<ul>

    {foreach from=$tasks item=task} 
        {if $task->completed}
            <li class="list-group-item item completed">
        {else}
            <li class="list-group-item">
        {/if}

        {$task->title} <small><a href="task/{$task->id}"> Details </a></small>
        {$task->title} <small><a href="delete/{$task->id}"> Delete </a></small>
        {if (!$task->completed)}
            {$task->title}<small><a href="completed/{$task->id}"> Completed </a></small>
        {/if}
        </li>
    {/foreach}

</ul>
    
{include 'templates/footer.tpl'}