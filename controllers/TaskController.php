<?php

include_once 'models/TaskModel.php';
include_once 'views/TaskView.php';

class TaskController {

    private $modelTask;
    private $modelPeople;
    private $view;

    function __construct() {
        $this->modelTask = new TaskModel();
        $this->modelPeople = new TaskPeople();
        
        $this->view = new TaskView();
    }

    function delete($id) {
        $this->model->delete($id);
        header("Location: ../tasks");
    }

    function showDetail($id) {

        $task = $this->model->get($id);
        $this->view->showDetail($task);       
    }    


    /**
     * Construye un HTML de la vista de tareas
     * @return String
     */
    function showTasks() {
        $tasks = $this->model->getAll();
        $this->view->showAll($tasks);


    }


    /**
     * Crea una tarea en base a los datos cargados en el formulario
     */
    public function newTask() {
        $title = $_POST['title'];
        $priority = $_POST['priority'];
        $description = $_POST['description'];

        if (!empty($title) && !empty($priority)) {
            if ($_FILES['input_name']['type'] == "image/jpg" || $_FILES['input_name']['type'] == "image/jpeg" 
                || $_FILES['input_name']['type'] == "image/png") {
                    $this->model->new($title, $priority, $description , $_FILES['input_name']['tmp_name']);
            } else {
                $this->model->new($title, $priority, $description);
            }
            header("Location: ./tasks");
        } else {
            echo '<h1>Error! Faltan completar datos. </h1>';
        }    
    }

    /**
     * @param $id
     * Finalizar una tarea
     */
    public function endTask($id) {
        $this->model->end($id);
        header("Location: ../tasks");
    }    
}