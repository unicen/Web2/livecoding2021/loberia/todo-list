<?php

require_once('View.php');
include_once('helpers/auth.helper.php');

class TaskView extends View {

    public function __construct() {
        parent::__construct();
        $authHelper = new AuthHelper();
        $username = $authHelper->getLoggedUserName();
        $this->getSmarty()->assign('username', $username);
    }

    /**
     * @param $task 
     * Muestra una tarea pasada por parámetro
    **/
    function showDetail($task) {
        $this->getSmarty()->assign('title', "Detalle de Tarea");
        $this->getSmarty()->assign('task', $task);
        $this->getSmarty()->display('templates/taskDetail.tpl'); // muestro el template   
    }

    /**
     * @return string
     * Contruye el html para visualizar las tareas guardadas en la bd
    **/
    function showAll($tasks) {
        $this->getSmarty()->assign('title', "Task List");
        $this->getSmarty()->assign('tasks', $tasks);
        $this->getSmarty()->assign('home', BASE_URL.'tasks');
        $this->getSmarty()->display('templates/tasks.tpl'); // muestro el template   
    }

    /**
    * Muestra errores por pantalla
    **/
    public function showError($msg) {
        $this->getSmarty()->assign('message', $msg);
        $this->getSmarty()->assign('title', 'Error');
        $this->getSmarty()->assign('home', BASE_URL.'tasks');

        $this->getSmarty()->display('templates/error.tpl');
    }

}