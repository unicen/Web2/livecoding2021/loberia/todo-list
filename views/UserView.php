<?php

require_once('View.php');

class UserView extends View {

    public function __construct() {
        parent::__construct();
    }

    public function showLogin($error=null) {
        $this->getSmarty()->assign('title', "Login");
        $this->getSmarty()->assign('error', $error);        
        $this->getSmarty()->assign('home', BASE_URL.'tasks');
        $this->getSmarty()->display('templates/login.tpl');
    }

    public function showRegistrar($error=null) {
        $this->getSmarty()->assign('title', "Login");
        $this->getSmarty()->assign('error', $error);        
        $this->getSmarty()->assign('home', BASE_URL.'tasks');
        $this->getSmarty()->display('templates/registrar.tpl');
    }    
}